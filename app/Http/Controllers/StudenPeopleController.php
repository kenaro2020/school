<?php

namespace App\Http\Controllers;

use App\Models\StudenPeople;
use App\Models\Student;
use App\Models\People;
use Illuminate\Http\Request;

class StudenPeopleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        if($request){
            $idLevel = $request->level_id;
            $idSection = $request->section_id;
        }
        $students = Student::where('is_active',1)->get();
        $peoples = People::all();
        return view('student_people.index',compact('students','peoples'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }
    public function student_people(){
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $students = $request->get('student_id');
        $peoples = $request->get('people_id');
        $cont = 0;
        
        while($cont < count($students)){
            $student_people = new StudenPeople();
            $student_people->student_id = $students[$cont];
            $student_people->people_id = $peoples[$cont];
            $student_people->save();
            $cont++;
        }
        return redirect('student_people');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\StudenPeople  $studenPeople
     * @return \Illuminate\Http\Response
     */
    public function show(StudenPeople $studenPeople)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\StudenPeople  $studenPeople
     * @return \Illuminate\Http\Response
     */
    public function edit(StudenPeople $studenPeople)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\StudenPeople  $studenPeople
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, StudenPeople $studenPeople)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\StudenPeople  $studenPeople
     * @return \Illuminate\Http\Response
     */
    public function destroy(StudenPeople $studenPeople)
    {
        //
    }
}
