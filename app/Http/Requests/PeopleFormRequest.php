<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PeopleFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'fullname' => 'required|min:3',
            'cui' => 'required|min:13|max:13',
        ];
    }
    public function messages()
    {
        return [
            "fullname.required" => "El nombre del estudiante es obligatorio",
            "fullname.min" => "Ingrese un nombre válido",
            "cui.min" => "El C.U.I. contiene 13 carácteres",
            "cui.max" => "El C.U.I. contiene 13 carácteres",
        ];
    }
}
