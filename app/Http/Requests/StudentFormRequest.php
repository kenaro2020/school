<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StudentFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'fullname' => 'required|min:3',
            'code' => 'required|unique:students,code|min:7',
            'cui' => 'required|min:13|max:13|unique:students,cui',
        ];
    }
    public function messages()
    {
        return [
            "fullname.required" => "El nombre del estudiante es obligatorio",
            "fullname.min" => "Ingrese un nombre válido",
            "code.unique" => "El código ingresado ya existe en los registros",
            "code.required" => "El código es obligatorio",
            "code.min" => "El código debe incluir mínimo 7 carácteres",
            "cui.min" => "El C.U.I. contiene 13 carácteres",
            "cui.max" => "El C.U.I. contiene 13 carácteres",
            "cui.unique" => "El C.U.I. ya está registrado"
        ];
    }
    
}
