<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StudenPeople extends Model
{
    use HasFactory;
    protected $table = "studen_people";
    protected $fillable = ['student_id','people_id'];
}
