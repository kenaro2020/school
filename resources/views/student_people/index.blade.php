@extends('layouts.app');
@section('content')
<div class="container">
    <div class="row">
        <div class="col text-center shadow-lg p-3 mb-5 bg-body rounded">
            <h3>Asignar Padres</h3>
            <hr>
            
            <div class="input-group mb-3 input-group-lg">
                <span class="input-group-text" id="inputGroup-sizing-lg">Estudiante</span>
                <select id="student_id" class="form-control" required>
                <option value="" disabled selected>--Elegir un Estudiante --</option>
                @foreach($students as $student)
                    <option value="{{$student->id}}_{{$student->fullname}}">{{$student->fullname}}</option>
                @endforeach
                </select>
            </div>
            <div class="input-group mb-3 input-group-lg">
                <span class="input-group-text" id="inputGroup-sizing-lg">Encargado</span>
                <select id="people_id" class="form-control" required>
                  <option value="" disabled selected>--Elegir un Encargado --</option>
                  @foreach($peoples as $people)
                      <option value="{{$people->id}}_{{$people->fullname}}">{{$people->fullname}}</option>
                  @endforeach
                </select>
            </div>
            <button type="button" id="addStudent" class="btn btn-primary">Agregar</button>
            <button type="reset" class="btn btn-danger">Cancelar</button>
        </div>
        <div class="col-md-9 text-center shadow-lg p-3 mb-5 bg-body rounded">
            <h3>Asignación de Padres</h3>
            <hr>
            <form action="{{route('student_people.store')}}" method="post">
            @csrf
                <table class="table table-striped table-hover">
                    <thead>
                        <tr>
                            <th scope="row">Hijo(a)</th>
                            <th scope="row">Encargado</th> 
                            <th scope="row">Opciones</th>
                        </tr>
                    </thead>
                    <tbody id="tableAdd">
                        
                    </tbody>
                    <tfoot id="btnRegister">
                        <tr>
                            <td></td>
                            <td>
                                <button class="btn btn-success">Registrar Parentesco</button>
                            </td>
                        </tr>
                    </tfoot>
                </table>
            </form>
        </div>
    </div>
</div>
@push('scripts')
<script>
    cont = 0;
    $(document).ready(function(){
        validar(false);
    });
    $('#people_id').change(datos);
    function datos(){
        var data_student = document.getElementById('student_id').value.split('_');
        var data_person = document.getElementById('people_id').value.split('_');
        fullname_student = data_student[1];
        idStudent = data_student[0];
        fullname_people = data_person[1];
        idPeople = data_person[0];
        var fila = "<tr id='fila"+cont+"'>"+
                   "<td>"+fullname_student+"</td><input type='hidden' name='student_id[]' value='"+idStudent+"'>"+
                   "<td>"+fullname_people+"</td><input type='hidden' name='people_id[]' value='"+idPeople+"'>"+
                   "<td><button type='button' onclick='eliminar("+cont+");' <i class='fa-solid fa-trash'></i></button></td>"+
                   "</tr>";
        cont++;
        $('#tableAdd').append(fila);
        estado = true;
        validar(estado);
        
    }
    function validar(estado){
        if(estado == false){
            $('#btnRegister').hide();
        }else if(estado = true){
            $('#btnRegister').show();
        }
    }
    function eliminar(index){
        $('#fila'+index).remove();
    }
</script>
@endpush
@endsection