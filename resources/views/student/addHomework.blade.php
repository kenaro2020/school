<div class="modal fade" id="addHomework-{{$student->id}}">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header btn btn-primary">
          <h5 class="display-6">Trabajos Calificados</h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
          <h4>Estudiante: {{$student->fullname}}</h4><hr>
          <table class="table table-striped table-hover">
            <thead>
              <tr>
                <th>No.</th>
                <th>Materia</th>
                <th>Catedrático</th>
              </tr>
            </thead>
            @foreach($matters as $ma)
              @foreach($student_levels as $sl)
                @if($sl->student_id == $student->id)
                  @if($sl->level_section->matter->id == $ma->id)
                    <tbody>
                      <tr>
                        <td>{{$loop->iteration}}</td>
                        <td><button type="button" data-bs-toggle="modal" data-bs-whatever="{{$ma->id}}" data-bs-target="#nota-{{$student->id}}" data-bs-dismiss="modal fade" class="btn btn-info">{{$sl->level_section->matter->name}}</button></td>
                        <td>{{$sl->level_section->teacher->fullname}}</td>
                      </tr>
                      <tr>
                        <th scope="row">No.</th>
                        <th scope="row">Trabajo</th>
                        <th scope="row">Puntos</th>
                        @php $total_puntos = 0; $total_matter = 0; @endphp
                        @foreach($student_homeworks as $sh)
                          @if($sh->student_id == $student->id)
                            @if($sh->homework->matter_id == $ma->id)
                              <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>{{$sh->homework->name}}</td>
                                <td>{{$sh->points}}/{{$sh->homework->points}}</td>
                              </tr>
                              @php $total_puntos = $total_puntos + $sh->points @endphp
                              @php $total_matter = $total_matter + $sh->homework->points @endphp
                            @endif
                          @endif
                        @endforeach
                        <tr>
                          <td></td>
                          @if($total_puntos<59)<td style="background: red; color:white;">Rendimiento Bajo</td><td style="background: red; color:white"> 
                          @elseif($total_puntos>=60 && $total_puntos<=70)<td style="background: #fcba03; color:white;">Rendimiento Regular</td> <td style="background: #fcba03"> 
                          @elseif($total_puntos>=71 && $total_puntos<=90)<td style="background: yellow;">Rendimiento Aceptado</td> <td style="background:yellow;">
                          @elseif($total_puntos>=91 && $total_puntos<=100)<td style="background: green; color:white;">Excelente</td> <td style="background:green; color:white">
                          @endif
                          {{$total_puntos}}/{{$total_matter}}</td>
                          
                        </tr>
                        
                      </tr>
                    </tbody>
                  @endif
                @endif
              @endforeach
            @endforeach
          </table>
        </div>
      </div>
    </div>
  </div>