@extends('layouts.app');
@section('content')
<div class="container">
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <div class="row">
        <div class="col text-center shadow-lg p-3 mb-5 bg-body rounded">
            <h3>Nuevo Estudiante</h3>
            <hr>
            <form action="{{route('student.store')}}" method="post" class="form">
                @csrf
                <div class="input-group mb-3 input-group-lg">
                    <span class="input-group-text" id="inputGroup-sizing-lg"><i class="fa-sharp fa-solid fa-barcode"></i></span>
                    <input type="text" name="code" class="form-control" placeholder="Código de Alumno" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-lg" required>
                </div>
                <div class="input-group mb-3 input-group-lg">
                    <span class="input-group-text" id="inputGroup-sizing-lg"><i class="fa-solid fa-user"></i></span>
                    <input type="text" name="fullname" class="form-control" placeholder="Nombre Completo" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-lg" required>
                </div>
                <div class="input-group mb-3 input-group-lg">
                    <span class="input-group-text" id="inputGroup-sizing-lg"><i class="fa-solid fa-address-card"></i></span>
                    <input type="text" name="cui" class="form-control" placeholder="C.U.I." onkeypress="return validar(event);" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-lg" required>
                </div>
                <div class="input-group mb-3 input-group-lg">
                    <span class="input-group-text" id="inputGroup-sizing-lg"><i class="fa-solid fa-cake-candles"></i></span>
                    <input type="date" name="birthday" max="2017-01-01" class="form-control" placeholder="Fecha de Cumpleaños" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-lg" required>
                </div>
                <br><br><br><br><br>
               
                <hr>
                <button type="submit" class="btn btn-success">Guardar</button>
                <button class="btn btn-danger">Cancelar</button>
            </form>
        </div>
        <div class="col-md-9 text-center shadow-lg p-3 mb-5 bg-body rounded">
            <h3>Listado de Estudiantes</h3>
            <hr>
            @if(isset($students))
                <table class="table table-striped table-hover">
                    <thead class="bg-primary text-white">
                        <tr>
                            <th scope="row">No.</th>
                            <th scope="row">Código</th>
                            <th scope="row">Nombre</th>
                            <th scope="row">Edad</th>
                            <th scope="row">C.U.I.</th>
                            <th scope="row">Acción</th>
                        </tr>
                    </thead>
                    @foreach($students as $student)
                    <tr>
                        <td>{{$loop->iteration}}</td>
                        <td>{{$student->code}}</td>
                        <td><a class="nav nav-link" data-bs-target="#addHomework-{{$student->id}}" data-bs-toggle="modal">{{$student->fullname}}</a></td>
                        <td>{{\Carbon\Carbon::parse($student->birthday)->age}} años.</td>
                        <td>{{$student->cui}}</td>
                        <td>
                            <a href="" data-bs-toggle="modal" data-bs-target="#editarStudent-{{$student->id}}"><i data-fa-symbol="edit" class="fa-solid fa-pencil fa-fw"></i></a>
                            @if(Auth::user()->type_user->name == 'administrador')
                            <a href="" data-bs-toggle="modal" data-bs-target="#eliminarStudent-{{$student->id}}"><i data-fa-symbol="delete" class="fa-solid fa-trash fa-fw" style="color:red"></i></a>
                            @endif
                            <a href="{{action('StudentController@print_nota',$student->id)}}" ><i data-fa-symbol="delete" class="fa-solid fa-print fa-fw" style="color:green"></i></a>
                        </td>
                        <td>
                            @include('student.addHomework')
                        </td>
                    </tr>
                    
                    
                        @include('student.editar')
                        @include('student.delete')
                        @endforeach
                    </table>
                {{ $students->links() }}
            @else
                <h3>No Existen Registros!</h3>
                <h5><--- Agrega uno </h5>
            @endif
        </div>
    </div>
</div>

@endsection