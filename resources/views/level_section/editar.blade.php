<div class="modal fade" id="editarLevel-{{$levels->id}}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header btn btn-primary">
          <h5 class="display-6">Editar</h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
            <form action="{{route('level_section.update',$levels->id)}}" method="post" class="form">
                <input name="_method" type="hidden" value="PATCH">
                @csrf
                <div class="input-group mb-3 input-group-lg">
                  <span class="input-group-text" id="inputGroup-sizing-lg">Grado</span>
                  <select name="level_id" id="level_id" class="form-control">
                    <option value="" disabled selected>--Elegir un grado--</option>
                    @foreach($leveles as $level)
                        <option value="{{$level->id}}" {{($levels->level_id == $level->id) ? 'selected' : ''}}> {{$level->number->number}} - {{$level->name}}</option>
                    @endforeach
                  </select>
                </div>
                <div class="input-group mb-3 input-group-lg">
                    <span class="input-group-text" id="inputGroup-sizing-lg">Sección</span>
                    <select name="section_id" id="section_id" class="form-control">
                        <option value="" disabled selected>--Elegir una sección--</option>
                        @foreach($sections as $section)
                            <option value="{{$section->id}}" {{($levels->section_id == $section->id) ? 'selected' : ''}}>{{$section->name}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="input-group mb-3 input-group-lg">
                    <span class="input-group-text" id="inputGroup-sizing-lg">Materias</span>
                    <select name="matter_id" id="matter_id" class="form-control">
                        <option value="" disabled selected>--Elegir una materia--</option>
                        @foreach($matters as $matter)
                            <option value="{{$matter->id}}" {{($levels->matter_id == $matter->id) ? 'selected' : ''}}>{{$matter->name}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="input-group mb-3 input-group-lg">
                    <span class="input-group-text" id="inputGroup-sizing-lg">Ciclo</span>
                    <select name="cicle_id" id="cicle_id" class="form-control">
                        <option value="" disabled selected>--Elegir un ciclo--</option>
                        @foreach($cicles as $cicle)
                            <option value="{{$cicle->id}}" {{($levels->cicle_id == $cicle->id) ? 'selected' : ''}}>{{$cicle->cicle}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="input-group mb-3 input-group-lg">
                    <span class="input-group-text" id="inputGroup-sizing-lg">Maestro</span>
                    <select name="teacher_id" id="teacher_id" class="form-control">
                        <option value="" disabled selected>--Elegir un maestro--</option>
                        @foreach($teachers as $teacher)
                            <option value="{{$teacher->id}}" {{($levels->teacher_id == $teacher->id) ? 'selected' : ''}}>{{$teacher->fullname}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="input-group mb-3 input-group-lg">
                    <span class="input-group-text" id="inputGroup-sizing-lg">Periodo</span>
                    <select name="period_id" id="period_id" class="form-control">
                        <option value="" disabled selected>--Elegir un periodo--</option>
                        @foreach($periods as $period)
                            <option value="{{$period->id}}" {{($levels->period_id == $period->id) ? 'selected' : ''}}>{{$period->number->number}} - {{$period->period}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="input-group input-group-lg">
                    <span class="input-group-text" id="inputGroup-sizing-lg">Descripción</span>
                    <textarea name="description" id="description" cols="30" rows="10" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-lg">{{$levels->description}}</textarea>
                </div>
                <button type="button" class="btn btn-danger" data-bs-dismiss="modal">Cancelar</button>
                <button class="btn btn-primary">Editar</button>
            </form>
        </div>
      </div>
    </div>
  </div>