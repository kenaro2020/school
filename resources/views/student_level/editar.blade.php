<div class="modal fade" id="editarSL-{{$sh->id}}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header btn btn-primary">
          <h5 class="display-6">Editar Asignatura</h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
          <form action="{{route('student_level.update',$sh->id)}}" method="post" class="form">
            <input name="_method" type="hidden" value="PATCH">
            @csrf
            <div class="input-group mb-3 input-group-lg">
                <span class="input-group-text" id="inputGroup-sizing-lg">Estudiante</span>
                <select name="student_id" id="student_id" class="form-control">
                  <option value="" disabled selected>--Elegir un trabajo --</option>
                  @foreach($students as $student)
                      <option value="{{$student->id}}" {{($sh->student_id == $student->id) ? 'selected' : ''}}>{{$student->fullname}}</option>
                  @endforeach
                </select>
            </div>
            <div class="input-group mb-3 input-group-lg">
              <span class="input-group-text" id="inputGroup-sizing-lg">Grado Asignar</span>
              <select name="level_section_id" id="level_section_id" class="form-control">
                <option value="" disabled selected>--Elegir un trabajo --</option>
                @foreach($level_sections as $ls)
                    <option value="{{$ls->id}}" {{($sh->level_section_id == $ls->id) ? 'selected' : ''}}>{{$ls->level->number->number}}{{$ls->level->name}}-{{$ls->section->name}}-{{$ls->matter->name}}</option>
                @endforeach
              </select>
            </div>
            <button type="button" class="btn btn-danger" data-bs-dismiss="modal">Cancelar</button>
            <button class="btn btn-primary">Editar</button>
          </form>
        </div>
      </div>
    </div>
  </div>